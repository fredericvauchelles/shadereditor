﻿using System;
using Microsoft.Extensions.CommandLineUtils;

namespace Lightning.BuildSystem
{
    class BuildApplication
    {
        CommandLineApplication m_CLIApp;

        public BuildApplication()
        {
            m_CLIApp = new CommandLineApplication();
            RegisterApplication(m_CLIApp);
        }

        public void Execute(string[] args)
        {
            m_CLIApp.Execute(args);
        }

        void RegisterApplication(CommandLineApplication app)
        {
            app.Name = "dotnet li";

            app.HelpOption("-?|-h|--help");

            app.Command("template", RegisterTemplateCommand);
            app.Command("sample", RegisterSampleCommand);
        }

        void RegisterSampleCommand(CommandLineApplication app)
        {
            app.Description = "Sample commands";
            app.HelpOption("-?|-h|--help");

            app.Command("run", runApp =>
            {
                runApp.Description = "Run sample project renderer";

                var sampleNameArgument = runApp.Argument(
                    "[sampleName]",
                    "Name of the sample project"
                );

                runApp.OnExecute(() =>
                {
                    var project = new RootProject(Environment.CurrentDirectory);
                    var success = project.RunSample(sampleNameArgument.Value);
                    return success ? 0 : 1;
                });
            });

            app.Command("prepare", prepareApp =>
            {
                prepareApp.Description = "Prepare sample project renderer";

                var sampleNameArgument = prepareApp.Argument(
                    "[sampleName]",
                    "Name of the sample project"
                );

                var configurationOption = prepareApp.Option(
                    "-c|--configuration <configuration>",
                    "Configuration for building",
                    CommandOptionType.SingleValue
                );

                prepareApp.OnExecute(() =>
                {
                    var configuration = configurationOption.HasValue() ? configurationOption.Value() : "Debug";
                    var project = new RootProject(Environment.CurrentDirectory);
                    var success = project.PrepareSample(sampleNameArgument.Value, configuration);
                    return success ? 0 : 1;
                });
            });
        }

        void RegisterTemplateCommand(CommandLineApplication app)
        {
            app.Description = "Template commands";
            app.HelpOption("-?|-h|--help");

            app.Command("prepare", prepareApp =>
            {
                prepareApp.Description = "Prepare template project";
                prepareApp.HelpOption("-?|-h|--help");

                var configurationOption = prepareApp.Option(
                    "-c|--configuration <configuration>",
                    "Configuration to use for build (default: Debug)",
                    CommandOptionType.SingleValue
                );

                prepareApp.OnExecute(() =>
                {
                    var configuration = configurationOption.HasValue() ? configurationOption.Value() : "Debug";
                    var project = new RootProject(Environment.CurrentDirectory);
                    var success = project.PrepareTemplate(configuration);
                    return success ? 0 : 1;
                });
            });

            app.Command("package", packageApp =>
            {
                packageApp.Description = "Package template project";
                packageApp.HelpOption("-?|-h|--help");

                var configurationOption = packageApp.Option(
                    "-c|--configuration <configuration>",
                    "Configuration to use for build (default: Debug)",
                    CommandOptionType.SingleValue
                );

                packageApp.OnExecute(() =>
                {
                    var configuration = configurationOption.HasValue() ? configurationOption.Value() : "Debug";
                    var project = new RootProject(Environment.CurrentDirectory);
                    var success = project.PackageTemplate(configuration);
                    return success ? 0 : 1;
                });
            });
        }
    }
}
