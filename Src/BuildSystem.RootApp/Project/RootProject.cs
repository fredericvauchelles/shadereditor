﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using static Lightning.Logger;

namespace Lightning.BuildSystem
{
    class RootProject
    {
        Project.Project ProjectTemplate => new Project.Project(Path.Combine(RootDirectory, "ProjectTemplate"));
        string ProjectCommonsPath => Path.Combine(RootDirectory, @"Src\Commons");
        string ProjectEnginePath => Path.Combine(RootDirectory, @"Src\Engine");
        string ProjectAppPath => Path.Combine(RootDirectory, @"Src\App");
        string ProjectBuildAppPath => Path.Combine(RootDirectory, @"Src\BuildSystem.ProjectApp");
        string SamplesPath => Path.Combine(RootDirectory, "Samples");
        string OutPath => Path.Combine(RootDirectory, "Out");
        string OutProjectTemplatePackagePath => Path.Combine(OutPath, "ProjectTemplate.zip");
        
        Project.Project GetSampleProject(string sampleName) => new Project.Project(Path.Combine(SamplesPath, sampleName));

        public string RootDirectory { get; }

        public RootProject(string rootDirectory)
        {
            RootDirectory = rootDirectory;

            Trace.Listeners.Clear();
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out)
            {
                Name = "Console Logger",
                TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime
            });
        }

        public bool PrepareTemplate(string configuration)
        {
            PrepareProject(ProjectTemplate, configuration);
            return true;
        }

        static readonly Regex s_PackageTemplateExcludedFiles = new Regex(@"(?:Out\|\.log.txt|\.vs)");
        public bool PackageTemplate(string configuration)
        {
            if (File.Exists(OutProjectTemplatePackagePath))
                File.Delete(OutProjectTemplatePackagePath);
            if (!Directory.Exists(OutPath))
                Directory.CreateDirectory(OutPath);

            var projectTemplate = ProjectTemplate;

            using (var filestream = new FileStream(OutProjectTemplatePackagePath, FileMode.Create))
            using (var archive = new ZipArchive(filestream, ZipArchiveMode.Create))
            {
                foreach (var path in Directory.EnumerateFiles(projectTemplate.RootDirectory, "*.*", SearchOption.AllDirectories)
                    .Where(p => !s_PackageTemplateExcludedFiles.Match(p).Success))
                {
                    archive.CreateEntryFromFile(path, Path.GetRelativePath(projectTemplate.RootDirectory, path));
                }
            }

            return true;
        }

        public bool RunSample(string sampleName)
        {
            var project = GetSampleProject(sampleName);
            project.Run();

            return true;
        }

        public bool PrepareSample(string sampleName, string configuration)
        {
            var project = GetSampleProject(sampleName);
            PrepareProject(project, configuration);
            return true;
        }

        void PrepareProject(Project.Project project, string configuration)
        {
            var cwd = Environment.CurrentDirectory;

            LogInfo("Project", "Clear local NuGet repositories");
            DotNetUtils.Execute("nuget", "locals", "all", "--clear");

            LogInfo("Project", "Package dependencies nugets");
            Environment.CurrentDirectory = ProjectEnginePath;
            DotNetUtils.ExecuteAndThrowOnFail("pack", "-c", configuration, "-o", project.PackagesPath);
            Environment.CurrentDirectory = ProjectCommonsPath;
            DotNetUtils.ExecuteAndThrowOnFail("pack", "-c", configuration, "-o", project.PackagesPath);

            LogInfo("Project", "Publish App");
            Environment.CurrentDirectory = ProjectAppPath;
            DotNetUtils.ExecuteAndThrowOnFail("publish", "-c", configuration, "-o", project.EnginePath);

            LogInfo("Project", "Publish Build App");
            Environment.CurrentDirectory = ProjectBuildAppPath;
            DotNetUtils.ExecuteAndThrowOnFail("publish", "-c", configuration, "-o", project.ToolsPath);

            LogInfo("Project", "Update project's nugets");
            DotNetUtils.Execute("restore", project.PipelineCSProjPath);

            Environment.CurrentDirectory = cwd;
        }

        void RunProject(string projectPath)
        {
            var project = new Project.Project(projectPath);

            project.Run();
        }
    }
}
