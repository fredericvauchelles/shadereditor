﻿using Lightning.BuildSystem;

namespace BuildSystem.ProjectApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new BuildApplication();
            app.Execute(args);
        }
    }
}
