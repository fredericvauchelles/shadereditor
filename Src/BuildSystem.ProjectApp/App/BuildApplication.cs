﻿using System;
using Microsoft.Extensions.CommandLineUtils;

namespace Lightning.BuildSystem
{
    class BuildApplication
    {
        CommandLineApplication m_CLIApp;

        public BuildApplication()
        {
            m_CLIApp = new CommandLineApplication();
            RegisterApplication(m_CLIApp);
        }

        public void Execute(string[] args)
        {
            m_CLIApp.Execute(args);
        }

        void RegisterApplication(CommandLineApplication app)
        {
            app.Name = "dotnet li";

            app.HelpOption("-?|-h|--help");

            app.Command("run", runApp =>
            {
                runApp.Description = "Run sample project renderer";

                var sampleNameArgument = runApp.Argument(
                    "[sampleName]",
                    "Name of the sample project"
                );

                runApp.OnExecute(() =>
                {
                    var project = new Project.Project(Environment.CurrentDirectory);
                    var success = project.Run();
                    return success ? 0 : 1;
                });
            });
        }
            
    }
}
