﻿using Lightning.Assets;
using System;
using Veldrid;

namespace Lightning.RenderPipeline
{
    public interface IRenderPipeline : IDisposable
    {
        void CreateResources(ResourceFactory factory, GraphicsDevice gfxDevice, IAssetSystem assetSystem);
        void UpdateLogic(float deltaTime, InputSnapshot inputSnapshot, out bool exit);
        void RenderFrame(GraphicsDevice gfxDevice, float deltaTime);
        void OnAssetUpdated(AssetUpdated assetUpdated, out bool updatePipeline);
    }
}
