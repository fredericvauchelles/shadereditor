﻿using Lightning.Assets;
using System;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using Veldrid;

namespace Lightning.RenderPipeline
{
    public class DefaultRenderPipeline : IRenderPipeline
    {
        struct GlobalBuffer
        {
            public Vector4 Time;
            public Vector4 ScreenSize;
        }

        Shader m_ShaderVertex;
        Shader m_ShaderFragment;
        byte[] m_ShaderVertexBytes = Array.Empty<byte>();
        byte[] m_ShaderFragmentBytes = Array.Empty<byte>();
        Pipeline m_Pipeline;
        CommandList m_CommandList;

        ResourceLayout m_GlobalUniformBuffer;
        ResourceSet m_GlobalUniformResourceSet;
        DeviceBuffer m_UniformGlobals;

        float m_Time;

        public void CreateResources(ResourceFactory factory, GraphicsDevice gfxDevice, IAssetSystem assetSystem)
        {
            assetSystem.LoadShaderBytes("main.vert", ref m_ShaderVertexBytes);
            assetSystem.LoadShaderBytes("main.frag", ref m_ShaderFragmentBytes);

            if (m_ShaderVertexBytes.Length == 0
                || m_ShaderFragmentBytes.Length == 0)
            {
                return;
            }

            m_ShaderVertex = factory.CreateShader(new ShaderDescription(ShaderStages.Vertex, m_ShaderVertexBytes, "VS"));
            m_ShaderFragment = factory.CreateShader(new ShaderDescription(ShaderStages.Fragment, m_ShaderFragmentBytes, "FS"));

            var vertexLayout = new VertexLayoutDescription(
                new VertexElementDescription("Position", VertexElementSemantic.Position, VertexElementFormat.Float2),
                new VertexElementDescription("Color", VertexElementSemantic.Color, VertexElementFormat.Float4));

            m_UniformGlobals = factory.CreateBuffer(new BufferDescription((uint)Marshal.SizeOf<GlobalBuffer>(), BufferUsage.UniformBuffer));
            m_GlobalUniformBuffer = factory.CreateResourceLayout(new ResourceLayoutDescription(
                new ResourceLayoutElementDescription("Globals", ResourceKind.UniformBuffer, ShaderStages.Fragment)
            ));
            m_GlobalUniformResourceSet = factory.CreateResourceSet(new ResourceSetDescription(
                m_GlobalUniformBuffer,
                m_UniformGlobals
            ));

            var pipelineDescription = new GraphicsPipelineDescription
            {
                BlendState = BlendStateDescription.SingleOverrideBlend,
                DepthStencilState = DepthStencilStateDescription.DepthOnlyLessEqual,
                RasterizerState = RasterizerStateDescription.Default,
                PrimitiveTopology = PrimitiveTopology.TriangleStrip,
                ResourceLayouts = new [] { m_GlobalUniformBuffer },
                ShaderSet = new ShaderSetDescription(
                    vertexLayouts: new [] { vertexLayout }, 
                    shaders: new [] { m_ShaderVertex, m_ShaderFragment }
                    ),
                Outputs = gfxDevice.SwapchainFramebuffer.OutputDescription
            };
            m_Pipeline = factory.CreateGraphicsPipeline(pipelineDescription);

            m_CommandList = factory.CreateCommandList();
        }

        public void RenderFrame(GraphicsDevice gfxDevice, float deltaTime)
        {
            if (m_CommandList == null)
                return;

            m_Time += deltaTime;
            var frameBuffer = gfxDevice.SwapchainFramebuffer;
            var buffer = new GlobalBuffer
            {
                Time = new Vector4(
                    deltaTime,
                    m_Time,
                    1.0f / deltaTime, 0.0f
                ),
                ScreenSize = new Vector4(
                    frameBuffer.Width, frameBuffer.Height,
                    1.0f / frameBuffer.Width, 1.0f / frameBuffer.Height
                )
            };

            m_CommandList.Begin();
            
            m_CommandList.UpdateBuffer(m_UniformGlobals, 0, ref buffer);

            m_CommandList.SetFramebuffer(gfxDevice.SwapchainFramebuffer);
            m_CommandList.SetFullViewports();
            m_CommandList.SetFullScissorRects();
            m_CommandList.ClearColorTarget(0, RgbaFloat.Black);
            m_CommandList.SetPipeline(m_Pipeline);
            m_CommandList.SetGraphicsResourceSet(0, m_GlobalUniformResourceSet);
            m_CommandList.Draw(4);

            m_CommandList.End();
            gfxDevice.SubmitCommands(m_CommandList);
            gfxDevice.SwapBuffers();
        }

        public void OnAssetUpdated(AssetUpdated assetUpdated, out bool updatePipeline)
        {
            updatePipeline = assetUpdated.ShaderPaths.Contains("main.frag")
                || assetUpdated.ShaderPaths.Contains("main.vert");
        }

        public void UpdateLogic(float deltaTime, InputSnapshot inputSnapshot, out bool exit)
        {
            exit = false;
            for (int i = 0, c = inputSnapshot.KeyEvents.Count; i < c; ++i)
            {
                var k = inputSnapshot.KeyEvents[i];
                if (k.Key == Key.Escape)
                    exit = true;
            }
        }

        #region IDisposable Support
        bool m_DisposedValue; 

        void Dispose(bool disposing)
        {
            if (!m_DisposedValue)
            {
                if (disposing)
                {
                    m_GlobalUniformBuffer?.Dispose();
                    m_UniformGlobals?.Dispose();
                    m_GlobalUniformResourceSet?.Dispose();

                    m_CommandList?.Dispose();
                    m_Pipeline?.Dispose();
                    m_ShaderVertex?.Dispose();
                    m_ShaderFragment?.Dispose();
                }

                m_DisposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
