﻿using System.Collections.Generic;
using Veldrid;

namespace Lightning.Assets
{
    public static class ShaderSourceCodeUtils
    {
        public const string ExtShaderVertex = "vert";
        public const string ExtShaderFragment = "frag";
        public const string ExtShaderCompute = "comp";

        static readonly Dictionary<ShaderStages, string> ShaderStageExtensions = new Dictionary<ShaderStages, string>()
        {
            { ShaderStages.Fragment, ExtShaderFragment },
            { ShaderStages.Vertex, ExtShaderVertex },
            { ShaderStages.Compute, ExtShaderCompute },
        };

        public static string GetShaderStageExtension(ShaderStages stage)
        {
            return ShaderStageExtensions[stage];
        }

        public static ShaderStages GetShaderStageFromExtension(string extension)
        {
            switch (extension)
            {
                case ExtShaderCompute: return ShaderStages.Compute;
                case ExtShaderFragment: return ShaderStages.Fragment;
                case ExtShaderVertex: return ShaderStages.Vertex;
                default: return ShaderStages.None;
            }
        }
    }
}
