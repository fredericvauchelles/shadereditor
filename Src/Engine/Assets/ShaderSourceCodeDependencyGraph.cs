﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Veldrid;

namespace Lightning.Assets
{
    class ShaderSourceCodeDependencyGraph
    {
        static readonly Regex PragmaIncludeRegex = new Regex(@"^#include\s""(?<path>.*)""$");

        public struct ShaderSourceCodeFile : IEquatable<ShaderSourceCodeFile>
        {
            public string ProjectPath;
            public string ShaderPath;
            public ShaderStages Stage;

            public bool Equals(ShaderSourceCodeFile other)
            {
                return ShaderPath == other.ShaderPath
                    && ProjectPath == other.ProjectPath;
            }
        }

        class ShaderSourceCodeNode
        {
            public ShaderSourceCodeFile File;
            public HashSet<ShaderSourceCodeNode> IDependOnThem;
            public HashSet<ShaderSourceCodeNode> TheyDependOnMe;
        }

        List<ShaderSourceCodeNode> m_ShaderSourceCodeNodes = new List<ShaderSourceCodeNode>();
        Dictionary<string, int> m_ShaderSourceCodeIndexByProjectPath = new Dictionary<string, int>();

        public Project.Project Project { get; }
        

        public ShaderSourceCodeDependencyGraph(Project.Project project)
        {
            Project = project;
        }

        internal void ClearDependenciesOf(string[] projectPaths)
        {
            for (int i = 0, c = projectPaths.Length; i < c; ++i)
            {
                var p = projectPaths[i];
                if (!TryGetNodeByProjectPath(p, out ShaderSourceCodeNode node))
                    continue;

                foreach (var they in node.TheyDependOnMe)
                    they.IDependOnThem.Remove(node);

                node.IDependOnThem.Clear();
            }
        }

        internal void CalculateDependenciesOf(string[] projectPaths)
        {
            var includeShaderPaths = new HashSet<string>();
            var includeProjectPaths = new HashSet<ShaderSourceCodeFile>();
            var includeNodes = new HashSet<ShaderSourceCodeNode>();
            for (int i = 0, c = projectPaths.Length; i < c; ++i)
            {
                var projectPath = projectPaths[i];
                ParseSourceCodeForIncludes(projectPath, includeShaderPaths);
                FindProjectPaths(includeShaderPaths, includeProjectPaths);
                GetOrInsertNodes(includeProjectPaths, includeNodes);

                var node = GetOrInsertNode(projectPath);

                node.IDependOnThem.UnionWith(includeNodes);

                foreach (var they in includeNodes)
                    they.TheyDependOnMe.Add(node);
            }
        }

        internal void GetShaderSourceCodeFilesFor(string[] projectPaths, List<ShaderSourceCodeFile> shaderSourceCodeFiles)
        {
            shaderSourceCodeFiles.Clear();
            var result = new HashSet<ShaderSourceCodeFile>();

            var stack = new Stack<string>(projectPaths);
            var done = new HashSet<string>();

            while (stack.Count > 0)
            {
                var path = stack.Pop();
                done.Add(path);

                if (!TryGetNodeByProjectPath(path, out ShaderSourceCodeNode node))
                    continue;

                if (node.File.Stage != ShaderStages.None)
                    shaderSourceCodeFiles.Add(node.File);

                foreach (var deps in node.TheyDependOnMe
                    .Where(d => !done.Contains(d.File.ProjectPath)))
                {
                    stack.Push(deps.File.ProjectPath);
                }
            }

            shaderSourceCodeFiles.AddRange(result);
        }

        bool TryGetNodeByProjectPath(string projectPath, out ShaderSourceCodeNode node)
        {
            if (m_ShaderSourceCodeIndexByProjectPath.TryGetValue(projectPath, out int i))
            {
                node = m_ShaderSourceCodeNodes[i];
                return true;
            }
            else
            {
                node = null;
                return false;
            }
        }

        void GetOrInsertNodes(HashSet<ShaderSourceCodeFile> includeProjectPaths, HashSet<ShaderSourceCodeNode> includeNodes)
        {
            includeNodes.Clear();

            foreach (var path in includeProjectPaths)
            {
                var node = GetOrInsertNode(path.ProjectPath, path.ShaderPath);
                includeNodes.Add(node);
            }
        }

        ShaderSourceCodeNode GetOrInsertNode(string projectPath, string shaderPath = null)
        {
            shaderPath = string.IsNullOrEmpty(shaderPath) ? Project.GetShaderPath(projectPath) : shaderPath;

            if(!TryGetNodeByProjectPath(projectPath, out ShaderSourceCodeNode node))
            {
                var ext = Path.GetExtension(projectPath).Substring(1);
                node = new ShaderSourceCodeNode
                {
                    File = new ShaderSourceCodeFile
                    {
                        ProjectPath = projectPath,
                        ShaderPath = shaderPath,
                        Stage = ShaderSourceCodeUtils.GetShaderStageFromExtension(ext)
                    },
                    IDependOnThem = new HashSet<ShaderSourceCodeNode>(),
                    TheyDependOnMe = new HashSet<ShaderSourceCodeNode>()
                };
                AddNode(node);
            }
            return node;
        }

        void AddNode(ShaderSourceCodeNode node)
        {
            Debug.Assert(!m_ShaderSourceCodeIndexByProjectPath.ContainsKey(node.File.ProjectPath));

            m_ShaderSourceCodeNodes.Add(node);
            m_ShaderSourceCodeIndexByProjectPath[node.File.ProjectPath] = m_ShaderSourceCodeNodes.Count - 1;
        }

        void FindProjectPaths(HashSet<string> includeShaderPaths, HashSet<ShaderSourceCodeFile> includeProjectPaths)
        {
            includeProjectPaths.Clear();

            foreach (var shaderPath in includeShaderPaths)
            {
                foreach (var includeFolder in Project.Properties.ShaderCompilation.IncludeFolders)
                {
                    var absolutePath = Path.Combine(includeFolder, shaderPath);
                    if (!File.Exists(absolutePath))
                        continue;

                    includeProjectPaths.Add(new ShaderSourceCodeFile
                    {
                        ShaderPath = shaderPath,
                        ProjectPath = Project.GetRelativePath(absolutePath),
                        Stage = ShaderSourceCodeUtils.GetShaderStageFromExtension(Path.GetExtension(shaderPath))
                    });
                }
            }
        }

        void ParseSourceCodeForIncludes(string projectPath, HashSet<string> includeShaderPaths)
        {
            includeShaderPaths.Clear();

            var absoluteSourceCodePath = Project.GetAbsolutePath(projectPath);
            var lines = File.ReadAllLines(absoluteSourceCodePath);
            for (int i = 0, c = lines.Length; i < c; ++i)
            {
                var match = PragmaIncludeRegex.Match(lines[i]);
                if (match.Success)
                    includeShaderPaths.Add(match.Groups["path"].Value);
            }
        }
    }
}
