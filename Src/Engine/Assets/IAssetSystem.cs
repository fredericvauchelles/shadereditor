﻿using System;

namespace Lightning.Assets
{
    public class AssetUpdated
    {
        public string[] ShaderPaths;
    }

    public interface IAssetSystem
    {
        void LoadShaderBytes(string shaderPath, ref byte[] outByteCode);
    }
}
