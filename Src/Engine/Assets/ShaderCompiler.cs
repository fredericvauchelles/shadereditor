﻿using System.IO;
using Veldrid;

namespace Lightning.Assets
{
    public abstract class ShaderCompiler
    {
        public struct Error
        {
            public readonly string SourceCodeFile;
            public readonly string Message;
            public readonly int LineNumber;
            public readonly int ColNumber;

            public Error(
                string sourceCodeFile,
                string message,
                int lineNumber,
                int colNumber
            )
            {
                SourceCodeFile = sourceCodeFile;
                LineNumber = lineNumber;
                ColNumber = colNumber;
                Message = message;
            }
        }

        public struct Result
        {
            public readonly bool Success;
            public readonly string SourceCodeFile;
            public readonly string ByteCodeFile;
            public readonly Error[] Errors;
            public byte[] ByteCode => File.ReadAllBytes(ByteCodeFile);

            public Result(bool success, string sourceCodeFile, string byteCodeFile, Error[] errors)
            {
                Success = success;
                SourceCodeFile = sourceCodeFile;
                ByteCodeFile = byteCodeFile;
                Errors = errors;
            }
        }

        public string OutDirectory { get; }
        public string SrcDirectory { get; }

        protected Project.Project Project { get; }

        public ShaderCompiler(Project.Project project)
        {
            SrcDirectory = project.ShaderSourcePath;
            OutDirectory = project.ShaderBinaryPath;
            Project = project;
        }

        public abstract Result Compile(string shaderSourcePath, ShaderStages shaderStages);

        protected string GetShaderBinaryAbsolutePath(string shaderPath) => Path.Combine(OutDirectory, Project.GetRelativePath(shaderPath));
        protected string GetShaderSourceAbsolutePath(string shaderPath) => Path.Combine(SrcDirectory, shaderPath);
    }
}
