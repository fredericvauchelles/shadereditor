﻿using System;
using System.Collections.Generic;
using System.IO;
using Veldrid;

namespace Lightning.Assets
{
    public class AssetSystem : IAssetSystem
    {
        class ShaderAsset
        {
            public string Path;
            public byte[] ByteCode;
        }

        Dictionary<string, ShaderAsset> m_Shaders = new Dictionary<string, ShaderAsset>();

        public event Action<AssetUpdated> AssetUpdated;

        public string GetShaderExtension(GraphicsBackend backend)
        {
            switch (backend)
            {
                case GraphicsBackend.Direct3D11:
                    return "hlsl.bytes";
                case GraphicsBackend.Vulkan:
                    return "spv";
                case GraphicsBackend.OpenGL:
                    return "glsl";
                case GraphicsBackend.Metal:
                    return "metallib";
                default:
                    throw new System.InvalidOperationException();
            }
        }

        public bool LoadShaderFromFile(string path, string byteCodePath)
        {
            var fullPath = Path.Combine(System.AppContext.BaseDirectory, "Shaders", byteCodePath);
            try
            {
                var shaderBytes = File.ReadAllBytes(fullPath);
                SetShaderAsset(path, shaderBytes);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SetShaderAsset(string path, byte[] bytecode)
        {
            m_Shaders[path] = new ShaderAsset
            {
                Path = path,
                ByteCode = bytecode
            };
            AssetUpdated?.Invoke(new AssetUpdated { ShaderPaths = new [] { path } });
        }

        public void LoadShaderBytes(string shaderPath, ref byte[] outByteCode)
        {
            if (m_Shaders.TryGetValue(shaderPath, out ShaderAsset asset))
            {
                Array.Resize(ref outByteCode, asset.ByteCode.Length);
                Array.Copy(asset.ByteCode, outByteCode, asset.ByteCode.Length);
            }
            else
            {
                Array.Resize(ref outByteCode, 0);
            }
        }
    }
}
