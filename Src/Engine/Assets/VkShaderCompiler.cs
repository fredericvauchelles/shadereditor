﻿using Lightning.Project;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Veldrid;

namespace Lightning.Assets
{
    public class VkShaderCompiler : ShaderCompiler
    {
        static readonly Regex GLSLangValidatorShaderCompilerErrorRegex = new Regex(@"^ERROR:\s(?:(?<sourceFile>[\w\\\/\.] +):(?<line>\d *):(?<column>\d *)\s)?(?<message>.*)$");
        static readonly Regex GLSLCShaderCompilerErrorRegex = new Regex(@"^(?<sourceFile>[\w\\\/\.]+):(?<line>\d*):(?<message>.*)$");

        static string VulkanSDKPath => Environment.GetEnvironmentVariable("VK_SDK_PATH");
        static string GLSLangValidatorPath => Path.Combine(VulkanSDKPath, "Bin32\\glslangValidator.exe");
        static string GLSLCPath => "glslc.exe";

        public VkShaderCompiler(Project.Project project)
            : base(project)
        {
            
        }

        public override Result Compile(string shaderSourcePath, ShaderStages shaderStages)
        {
            return GLSLCCompile(shaderSourcePath, shaderStages);
        }

        Result GLSLangValidatorCompile(string shaderSourcePath, ShaderStages shaderStages)
        {
            var targetFile = GetShaderBinaryAbsolutePath(shaderSourcePath) + ".spv";
            var sourceFile = shaderSourcePath;

            var targetFileInfo = new FileInfo(targetFile);
            if (!targetFileInfo.Directory.Exists)
                targetFileInfo.Directory.Create();

            var args = new StringBuilder();
            args.Append("-V ");

            args.Append("-S ");
            args.Append(ShaderSourceCodeUtils.GetShaderStageExtension(shaderStages));
            args.Append(' ');

            args.Append("-o ");
            args.Append(targetFile);
            args.Append(' ');
            args.Append(sourceFile);

            var procInfo = new ProcessStartInfo
            {
                FileName = GLSLangValidatorPath,
                Arguments = args.ToString(),
                CreateNoWindow = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false
            };

            var proc = Process.Start(procInfo);
            var stdout = proc.StandardOutput.ReadToEnd();
            var stderr = proc.StandardError.ReadToEnd();

            proc.WaitForExit();

            var stdLines = stdout.Split('\n');
            var errors = new List<Error>();
            for (int i = 0, c = stdLines.Length; i < c; ++i)
            {
                var match = GLSLangValidatorShaderCompilerErrorRegex.Match(stdLines[i]);
                if (match.Success)
                {
                    errors.Add(new Error(
                        match.Groups["sourceFile"].Value,
                        match.Groups["message"].Value,
                        string.IsNullOrEmpty(match.Groups["line"].Value) ? 0 : int.Parse(match.Groups["line"].Value),
                        string.IsNullOrEmpty(match.Groups["column"].Value) ? 0 : int.Parse(match.Groups["column"].Value)
                    ));
                }
            }

            var result = new Result(proc.ExitCode == 0, shaderSourcePath, targetFile, errors.ToArray());

            return result;
        }

        Result GLSLCCompile(string shaderSourcePath, ShaderStages shaderStages)
        {
            var targetFile = GetShaderBinaryAbsolutePath(shaderSourcePath) + ".spv";
            var sourceFile = shaderSourcePath;

            var targetFileInfo = new FileInfo(targetFile);
            if (!targetFileInfo.Directory.Exists)
                targetFileInfo.Directory.Create();

            var args = new StringBuilder();

            args.Append("-o ");
            args.Append(targetFile);
            args.Append(' ');

            foreach (var includeFolder in Project.Properties.ShaderCompilation.IncludeFolders)
            {
                args.Append("-I \"");
                args.Append(includeFolder);
                args.Append("\" ");
            }
            args.Append(' ');

            foreach (var define in Project.Properties.ShaderCompilation.Defines)
            {
                args.Append("-D");
                args.Append(define);
                args.Append(' ');
            }

            args.Append("-DBACKEND=VULKAN ");

            args.Append(sourceFile);

            var procInfo = new ProcessStartInfo
            {
                FileName = GLSLCPath,
                Arguments = args.ToString(),
                CreateNoWindow = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false
            };

            var proc = Process.Start(procInfo);
            var stdout = proc.StandardOutput.ReadToEnd();
            var stderr = proc.StandardError.ReadToEnd();

            proc.WaitForExit();

            var stdLines = stderr.Split('\n');
            var errors = new List<Error>();
            for (int i = 0, c = stdLines.Length; i < c; ++i)
            {
                var match = GLSLCShaderCompilerErrorRegex.Match(stdLines[i]);
                if (match.Success)
                {
                    errors.Add(new Error(
                        match.Groups["sourceFile"].Value,
                        match.Groups["message"].Value,
                        string.IsNullOrEmpty(match.Groups["line"].Value) ? 0 : int.Parse(match.Groups["line"].Value),
                        0
                    ));
                }
            }

            var result = new Result(proc.ExitCode == 0, shaderSourcePath, targetFile, errors.ToArray());

            return result;
        }
    }
}
