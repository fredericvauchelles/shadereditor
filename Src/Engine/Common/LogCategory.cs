﻿namespace Lightning
{
    public enum LogCategory
    {
        Renderer,
        AssetPipeline,
        Logger
    }
}
