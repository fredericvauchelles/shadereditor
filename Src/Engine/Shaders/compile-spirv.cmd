@echo off

%VK_SDK_PATH%\bin\glslangvalidator -V -S vert main.vert.glsl -o main.vert.spv
%VK_SDK_PATH%\bin\glslangvalidator -V -S frag main.frag.glsl -o main.frag.spv