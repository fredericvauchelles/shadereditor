﻿using Lightning.Assets;
using System.Collections.Generic;
using static Lightning.Logger;

namespace Lightning.Project
{
    public class ProjectAutoCompiler
    {
        ProjectSourceCodeWatcher m_Project;
        AssetSystem m_AssetSystem;
        ShaderCompiler m_Compiler;
        ShaderSourceCodeDependencyGraph m_DependencyGraph;

        public ProjectAutoCompiler(
            ProjectSourceCodeWatcher project, 
            AssetSystem assetSystem, 
            ShaderCompiler compiler
        )
        {
            m_Project = project;
            m_AssetSystem = assetSystem;
            m_Compiler = compiler;

            m_DependencyGraph = new ShaderSourceCodeDependencyGraph(project.Project);
            m_Project.ShaderSourceChanged += ProjectOnShaderSourceChanged;
        }

        void ProjectOnShaderSourceChanged(ProjectSourceCodeWatcher.ShaderSourceChangedParams args)
        {
            m_DependencyGraph.ClearDependenciesOf(args.ProjectPaths);
            m_DependencyGraph.CalculateDependenciesOf(args.ProjectPaths);

            var toCompile = new List<ShaderSourceCodeDependencyGraph.ShaderSourceCodeFile>();
            m_DependencyGraph.GetShaderSourceCodeFilesFor(args.ProjectPaths, toCompile);

            for (int i = 0, c = toCompile.Count; i < c; ++i)
            {
                LogInfo(LogCategory.AssetPipeline, $"Compiling {toCompile[i].ShaderPath}");
                var result = m_Compiler.Compile(toCompile[i].ProjectPath, toCompile[i].Stage);

                if (result.Success)
                    m_AssetSystem.SetShaderAsset(toCompile[i].ShaderPath, result.ByteCode);
                else
                {
                    LogError(LogCategory.AssetPipeline, $"Failed to compile shader {toCompile[i].ShaderPath}");
                    for (int j = 0, d = result.Errors.Length; j < d; ++j)
                    {
                        var error = result.Errors[j];
                        LogError(LogCategory.AssetPipeline, $"Failed to compile shader {error.SourceCodeFile}:{error.LineNumber} {error.Message}");
                    }
                }
            }
        }
    }
}
