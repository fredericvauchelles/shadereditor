﻿using Lightning.Assets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Veldrid;

using static Lightning.Logger;

namespace Lightning.Project
{
    public class ProjectSourceCodeWatcher : IDisposable
    {
        public struct ShaderSourceChangedParams
        {
            public string[] ShaderPaths;
            public string[] ProjectPaths;
            public ShaderStages[] Stages;
        }

        public struct PipelineSourceChangedParams
        {
            public string[] ProjectPaths;
        }

        const int ThrottleTime = 100; // Wait 100ms to receive all events
        const string ShaderSourceSearchPattern = "*.*";
        const string PipelineSourceSearchPattern = "*.dll";

        List<FileSystemWatcher> m_ShaderSourceWatchers = new List<FileSystemWatcher>();
        FileSystemWatcher m_PipelineSourceWatcher;

        HashSet<string> m_ShaderSourceChangedBuffer = new HashSet<string>();
        HashSet<string> m_PipelineSourceChangedBuffer = new HashSet<string>();

        Task m_ShaderSourceTask;
        Task m_PipelineSourceTask;

        public event Action<ShaderSourceChangedParams> ShaderSourceChanged;
        public event Action<PipelineSourceChangedParams> PipelineSourceChanged;

        public Project Project { get; }

        public ProjectSourceCodeWatcher(Project project)
        {
            Project = project;
        }

        public void StartWatch()
        {
            WatchShaderSourceFolders(
                Project.Properties.ShaderCompilation.IncludeFolders
            );

            try
            {
                m_PipelineSourceWatcher = new FileSystemWatcher(Project.PipelineSourcePath);
                m_PipelineSourceWatcher.EnableRaisingEvents = true;
                m_PipelineSourceWatcher.NotifyFilter = NotifyFilters.LastWrite;
                m_PipelineSourceWatcher.IncludeSubdirectories = true;
                m_PipelineSourceWatcher.Filter = PipelineSourceSearchPattern;

                m_PipelineSourceWatcher.Changed += PipelineSourceWatcherOnChanged;

                LogInfo(LogCategory.AssetPipeline, $"Initial pipeline import at ({Project.PipelineSourcePath})");
                foreach (var file in Directory.EnumerateFiles(
                    Project.PipelineSourcePath,
                    PipelineSourceSearchPattern,
                    SearchOption.AllDirectories
                ))
                {
                    PipelineSourceWatcherOnChanged(
                        this,
                        new FileSystemEventArgs(
                            WatcherChangeTypes.Changed,
                            Path.GetDirectoryName(file),
                            Path.GetFileName(file)
                        )
                    );
                }
            }
            catch (Exception e)
            {
                LogWarning(LogCategory.AssetPipeline, $"Failed to watch pipeline source directory: {e.Message}");
            }
        }

        private void WatchShaderSourceFolders(IReadOnlyCollection<string> includeFolders)
        {
            foreach (var folder in includeFolders)
            {
                var absolutePath = Project.GetAbsolutePath(folder);
                try
                {
                    var watcher = new FileSystemWatcher(absolutePath)
                    {
                        EnableRaisingEvents = true,
                        NotifyFilter = NotifyFilters.LastAccess,
                        IncludeSubdirectories = true,
                        Filter = ShaderSourceSearchPattern
                    };
                    watcher.Changed += ShaderSourceWatcherOnChanged;
                    m_ShaderSourceWatchers.Add(watcher);
                }
                catch (Exception e)
                {
                    LogWarning(LogCategory.AssetPipeline, $"Failed to watch shader source directory: {e.Message}");
                }
            }

            foreach (var folder in includeFolders)
            {
                var absolutePath = Project.GetAbsolutePath(folder);
                try
                {
                    LogInfo(LogCategory.AssetPipeline, $"Initial shader import at {absolutePath}");
                    foreach (var file in Directory.EnumerateFiles(
                        absolutePath,
                        ShaderSourceSearchPattern,
                        SearchOption.AllDirectories
                    ))
                    {
                        ShaderSourceWatcherOnChanged(
                            this,
                            new FileSystemEventArgs(
                                WatcherChangeTypes.Changed,
                                Path.GetDirectoryName(file),
                                Path.GetFileName(file)
                            )
                        );
                    }
                }
                catch (Exception e)
                {
                    LogWarning(LogCategory.AssetPipeline, $"Failed to watch shader source directory ({absolutePath}): {e.Message}");
                }
            }
        }

        void PipelineSourceWatcherOnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                m_PipelineSourceChangedBuffer.Add(e.FullPath);
                if (m_PipelineSourceTask == null)
                    m_PipelineSourceTask = Task.Delay(ThrottleTime)
                        .ContinueWith(TriggerPipelineSourceEvent);
            }
        }

        void ShaderSourceWatcherOnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Changed)
            {
                m_ShaderSourceChangedBuffer.Add(e.FullPath);
                if (m_ShaderSourceTask == null)
                    m_ShaderSourceTask = Task.Delay(ThrottleTime)
                        .ContinueWith(TriggerShaderSourceEvent);
            }
        }

        void TriggerShaderSourceEvent(Task task)
        {
            m_ShaderSourceTask = null;
            var paths = m_ShaderSourceChangedBuffer
                .Select(p => new { FullPath = p, Extension = Path.GetExtension(p) })
                .Where(p => !string.IsNullOrEmpty(p.Extension))
                .Select(p => new { p.FullPath, Stage = ShaderSourceCodeUtils.GetShaderStageFromExtension(p.Extension.Substring(1)) })
                .ToArray();
            m_ShaderSourceChangedBuffer.Clear();

            var param = new ShaderSourceChangedParams
            {
                ShaderPaths = paths.Select(p => Project.GetShaderPath(p.FullPath)).ToArray(),
                ProjectPaths = paths.Select(p => Project.GetRelativePath(p.FullPath)).ToArray(),
                Stages = paths.Select(p => p.Stage).ToArray()
            };

            for (int i = 0, c = param.ShaderPaths.Length; i < c; ++i)
                LogInfo(LogCategory.AssetPipeline, $"Shader source changed: {param.ShaderPaths[i]}");

            ShaderSourceChanged?.Invoke(param);
        }

        void TriggerPipelineSourceEvent(Task task)
        {
            m_PipelineSourceTask = null;
            var paths = m_PipelineSourceChangedBuffer.ToArray();
            m_PipelineSourceChangedBuffer.Clear();

            var param = new PipelineSourceChangedParams
            {
                ProjectPaths = paths.Select(Project.GetRelativePath).ToArray(),
            };

            LogInfo(LogCategory.AssetPipeline, $"Pipeline source changed: {string.Join(@"\r\n", param.ProjectPaths)}");

            PipelineSourceChanged?.Invoke(param);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        void Dispose(bool disposing)
        {
            if (disposing)
            {
                for (int i = 0, c = m_ShaderSourceWatchers.Count; i < c; ++i)
                {
                    m_ShaderSourceWatchers[i].Changed -= ShaderSourceWatcherOnChanged;
                    m_ShaderSourceWatchers[i].Dispose();
                }
                    
                m_ShaderSourceWatchers.Clear();
                m_PipelineSourceWatcher?.Dispose();
                m_PipelineSourceWatcher = null;
            }
        }
    }
}
