﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Lightning
{
    class TraceTextWriter : TextWriter
    {
        public override Encoding Encoding => Console.OutputEncoding;

        public override void Write(char value)      { Trace.Write(value); }

        public override void Write(string value)    { Logger.LogInfo(string.Empty, value); }
    }
}
