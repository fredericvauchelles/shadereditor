﻿using System;
using System.Diagnostics;
using System.IO;

namespace Lightning
{
    public static class Logger
    {
        public static void SetupLogTracing(string logFile)
        {
            Console.SetOut(new TraceTextWriter());

            Trace.Listeners.Clear();

            Trace.Listeners.Add(new TextWriterTraceListener(Console.OpenStandardOutput())
            {
                Name = "Console Logger",
                TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime
            });

            if (!string.IsNullOrEmpty(logFile))
            {
                try
                {
                    Trace.Listeners.Add(new TextWriterTraceListener(new FileStream(logFile, FileMode.OpenOrCreate))
                    {
                        Name = "Text Logger",
                        TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime,
                    });

                }
                catch (IOException e)
                {
                    LogError("Logger", $"Failed to open log file: {e.Message}");
                }
            }

            Trace.AutoFlush = true;
        }

        public static void LogInfo(string category, string message)
        {
            Trace.TraceInformation($"[{category}] {message}");
        }

        public static void LogWarning(string category, string message)
        {
            Trace.TraceWarning($"[{category}] {message}");
        }

        public static void LogError(string category, string message)
        {
            Trace.TraceError($"[{category}] {message}");
        }

        public static void LogInfo<TType>(TType category, string message)
            where TType : struct, IComparable
        {
            Trace.TraceInformation($"[{category}] {message}");
        }

        public static void LogWarning<TType>(TType category, string message)
            where TType : struct, IComparable
        {
            Trace.TraceWarning($"[{category}] {message}");
        }

        public static void LogError<TType>(TType category, string message)
            where TType : struct, IComparable
        {
            Trace.TraceError($"[{category}] {message}");
        }
    }
}
