﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Lightning.Project
{
    [DataContract]
    public class ProjectProperties
    {
        public static ProjectProperties New()
        {
            var result = new ProjectProperties();
            result.m_ShaderCompilation = ShaderCompilationProperties.New();
            return result;
        }

        [DataMember(Name = "shaderCompilation")]
        ShaderCompilationProperties m_ShaderCompilation = new ShaderCompilationProperties();

        public ShaderCompilationProperties ShaderCompilation => m_ShaderCompilation;
    }

    [DataContract]
    public class ShaderCompilationProperties
    {
        public static ShaderCompilationProperties New()
        {
            var result = new ShaderCompilationProperties();
            result.LoadRuntimeDataFromSerializedData();
            return result;
        }

        [DataMember(Name ="includeFolders")]
        string[] m_SerializedIncludeFolders = new string[0];

        [DataMember(Name = "defines")]
        string[] m_SerializedDefines = new string[0];

        HashSet<string> m_IncludeFolders = new HashSet<string>();
        public IReadOnlyCollection<string> IncludeFolders => m_IncludeFolders;

        public IReadOnlyCollection<string> Defines => m_SerializedDefines;

        [OnDeserialized]
        void OnDeserialized(StreamingContext c)
        {
            LoadRuntimeDataFromSerializedData();
        }

        void LoadRuntimeDataFromSerializedData()
        {
            m_IncludeFolders = m_IncludeFolders ?? new HashSet<string>();
            m_SerializedIncludeFolders = m_SerializedIncludeFolders ?? new string[0];
            m_SerializedDefines = m_SerializedDefines ?? new string[0];

            m_IncludeFolders.Clear();
            m_IncludeFolders.Add(@"Src\Shaders");
            m_IncludeFolders.UnionWith(m_SerializedIncludeFolders);
        }

        [OnSerializing]
        void OnSerializing(StreamingContext c)
        {
            Array.Resize(ref m_SerializedIncludeFolders, m_IncludeFolders.Count - 1);
            var i = 0;
            foreach (var folder in m_IncludeFolders
                .Where(f => f != @"Src\Shaders"))
            {
                m_SerializedIncludeFolders[i] = folder;
                ++i;
            }
        }
    }
}
