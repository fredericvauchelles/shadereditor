﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

using static Lightning.Logger;

namespace Lightning.Project
{
    public class Project
    {
        public string EnginePath => Path.Combine(RootDirectory, "Engine");
        public string ToolsPath => Path.Combine(RootDirectory, "Tools");
        public string PackagesPath => Path.Combine(RootDirectory, "Packages");
        public string ShaderSourcePath => Path.Combine(RootDirectory, @"Src\Shaders");
        public string ShaderBinaryPath => Path.Combine(RootDirectory, @"Out\Shaders");
        public string PipelineSourcePath => Path.Combine(RootDirectory, @"Src\Pipeline");
        public string PipelineCSProjPath => Path.Combine(PipelineSourcePath, @"ProjectRenderPipeline.csproj");
        public string BinSDL2AppPath => Path.Combine(EnginePath, @"Lightning.App.dll");
        public string LogFilePath => Path.Combine(RootDirectory, "renderer.log.txt");

        string PropertiesPath => Path.Combine(RootDirectory, "project.json");

        public string GetRelativePath(string absolutePath) => Path.GetRelativePath(RootDirectory, absolutePath);
        public string GetAbsolutePath(string relativePath) => Path.IsPathRooted(relativePath)
            ? relativePath
            : Path.Combine(RootDirectory, relativePath);
        public string GetShaderPath(string absolutePath)
        {
            foreach (var includeFolder in Properties.ShaderCompilation.IncludeFolders)
            {
                var relativePath = Path.GetRelativePath(includeFolder, absolutePath);
                if (!relativePath.StartsWith(".."))
                    return relativePath.Replace("\\", "/");
            }
            return absolutePath;
        }
        public string GetPipelinePath(string absolutePath) => Path.GetRelativePath(PipelineSourcePath, absolutePath);

        public string RootDirectory { get; }
        public ProjectProperties Properties { get; private set; }

        public Project(string rootDirectory)
        {
            RootDirectory = rootDirectory;

            Properties = ProjectProperties.New();
        }

        public void LoadPropertiesFromDisk()
        {
            if (File.Exists(PropertiesPath))
            {
                FileStream fileStream = null;
                try
                {
                    fileStream = new FileStream(PropertiesPath, FileMode.Open);
                    var serializer = new DataContractJsonSerializer(typeof(ProjectProperties));

                    Properties = (ProjectProperties)serializer.ReadObject(fileStream);
                }
                catch (Exception e)
                {
                    LogWarning("Project", $"Failed to load project properties: {e.Message}");
                }
                finally
                {
                    fileStream?.Dispose();
                }
            }
        }
    }
}
