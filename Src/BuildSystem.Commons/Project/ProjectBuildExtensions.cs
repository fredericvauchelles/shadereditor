﻿using System;

namespace Lightning.BuildSystem
{
    public static class ProjectBuildExtensions
    {
        private static object projectPath;

        public static bool Run(this Project.Project project)
        {
            var cwd = Environment.CurrentDirectory;
            Environment.CurrentDirectory = project.RootDirectory;
            DotNetUtils.Execute(project.BinSDL2AppPath);
            Environment.CurrentDirectory = cwd;

            return true;
        }
    }
}
