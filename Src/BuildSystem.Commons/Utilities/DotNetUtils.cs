﻿using System;
using Microsoft.DotNet.Cli.Utils;

namespace Lightning.BuildSystem
{
    public static class DotNetUtils
    {
        public static void ExecuteAndThrowOnFail(string commandName, params string[] args)
        {
            var result = Command.CreateDotNet(commandName, args).Execute();
            if (result.ExitCode != 0)
                throw new InvalidOperationException(result.StdOut);
        }

        public static CommandResult Execute(string commandName, params string[] args)
        {
            return Command.CreateDotNet(commandName, args).Execute();
        }
    }
}
