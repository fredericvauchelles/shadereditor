﻿using Lightning.Rendering;

namespace Lightning
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var renderer = new SDL2App())
                renderer.Run();
        }
    }
}
