﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Veldrid;
using Veldrid.Sdl2;
using Veldrid.StartupUtilities;
using Veldrid.Utilities;
using System.Linq;
using System.Runtime.Loader;
using System.Reflection;
using Lightning.RenderPipeline;
using Lightning.Assets;
using Lightning.Project;

using static Lightning.Logger;

namespace Lightning.Rendering
{
    public class SDL2App : IDisposable
    {
        Sdl2Window m_Window;
        GraphicsDevice m_GraphicsDevice;

        IRenderPipeline m_RenderPipeline = new DefaultRenderPipeline();
        IRenderPipeline m_NextRenderPipeline;
        Assembly m_RenderPipelineAssembly;
        Assembly m_NextRenderPipelineAssembly;
        bool m_UpdateRenderPipeline;

        Thread m_RenderThread;
        bool m_CanRender = true;
        Semaphore m_RenderPipelineMutex = new Semaphore(1, 1);
        bool m_UpdateWindowSize;

        AssetSystem m_AssetSystem;
        ProjectSourceCodeWatcher m_ProjectSourceCodeWatcher;
        ProjectAutoCompiler m_AutoCompiler;
        Project.Project m_Project;

        public SDL2App()
        {
            m_AssetSystem = new AssetSystem();
        }

        public void SetRenderPipeline(IRenderPipeline renderPipeline)
        {
            m_NextRenderPipeline = renderPipeline;
            m_UpdateRenderPipeline = m_NextRenderPipeline != null
                && m_NextRenderPipeline != m_RenderPipeline;
        }

        public void Run()
        {
            Debug.Assert(m_Window == null);

            m_Project = new Project.Project(Environment.CurrentDirectory);
            m_Project.LoadPropertiesFromDisk();
            SetupLogTracing(m_Project.LogFilePath);
            LogInfo(LogCategory.Renderer, "Initialize renderer");

            var windowCI = new WindowCreateInfo()
            {
                X = 100,
                Y = 100,
                WindowWidth = 960,
                WindowHeight = 540,
                WindowTitle = "Shader Editor Renderer"
            };
            m_Window = VeldridStartup.CreateWindow(ref windowCI);
            m_Window.Resized += () => m_UpdateWindowSize = true;

            m_GraphicsDevice = VeldridStartup.CreateGraphicsDevice(m_Window, GraphicsBackend.Vulkan);

            var sw = Stopwatch.StartNew();
            var lastDate = sw.Elapsed.TotalSeconds;

            ResourceFactory factory = new DisposeCollectorResourceFactory(m_GraphicsDevice.ResourceFactory);
            LogInfo(LogCategory.AssetPipeline, "Loading default assets");
            LoadDefaultAssets();

            m_RenderPipeline.CreateResources(factory, m_GraphicsDevice, m_AssetSystem);
            m_RenderThread = new Thread(RenderLoop);
            m_RenderThread.Start();

            m_ProjectSourceCodeWatcher = new ProjectSourceCodeWatcher(m_Project);
            m_ProjectSourceCodeWatcher.PipelineSourceChanged += PipelineSourceChanged;
            m_AutoCompiler = new ProjectAutoCompiler(m_ProjectSourceCodeWatcher, m_AssetSystem, new VkShaderCompiler(m_Project));
            m_ProjectSourceCodeWatcher.StartWatch();

            m_AssetSystem.AssetUpdated += AssetSystemOnAssetUpdated;

            while (m_Window.Exists && !m_DisposedValue)
            {
                if (m_UpdateRenderPipeline)
                {
                    m_RenderPipelineMutex.WaitOne();
                    LogInfo(LogCategory.Renderer, $"Updating render pipeline to {m_NextRenderPipeline}");
                    m_UpdateRenderPipeline = false;

                    m_RenderPipeline?.Dispose();
                    m_RenderPipeline = null;
                    if (m_RenderPipelineAssembly != null)
                    {
                        // TODO: unload m_RenderPipelineAssembly assembly
                    }
                    m_RenderPipeline = m_NextRenderPipeline;
                    m_RenderPipelineAssembly = m_NextRenderPipelineAssembly;

                    try
                    {
                        m_RenderPipeline.CreateResources(factory, m_GraphicsDevice, m_AssetSystem);
                    }
                    catch (Exception e)
                    {
                        LogError(LogCategory.Renderer, $"Failed to create render pipeline {m_RenderPipeline} ({e.Message})");
                        LogInfo(LogCategory.Renderer, "Using default render pipeline");

                        m_RenderPipeline?.Dispose();
                        if (m_RenderPipelineAssembly != null)
                        {
                            // TODO: unload m_RenderPipelineAssembly assembly
                        }
                        m_RenderPipelineAssembly = null;
                        m_RenderPipeline = new DefaultRenderPipeline();
                        m_RenderPipeline.CreateResources(factory, m_GraphicsDevice, m_AssetSystem);
                    }
                    m_NextRenderPipelineAssembly = null;
                    m_NextRenderPipeline = null;
                    m_RenderPipelineMutex.Release();
                    sw.Restart();
                    lastDate = sw.Elapsed.TotalSeconds;
                }

                var date = sw.Elapsed.TotalSeconds;
                var deltaTime = (float)(date - lastDate);

                var inputSnapshot = m_Window.PumpEvents();
                bool exit;
                m_RenderPipeline.UpdateLogic(deltaTime, inputSnapshot, out exit);
                if (exit)
                    break;
            }

            m_ProjectSourceCodeWatcher.PipelineSourceChanged -= PipelineSourceChanged;
            m_ProjectSourceCodeWatcher.Dispose();
            m_ProjectSourceCodeWatcher = null;
            m_AssetSystem.AssetUpdated -= AssetSystemOnAssetUpdated;

            m_CanRender = false;
            m_RenderThread.Join();

            m_Window.Close();
            m_Window = null;
        }

        void PipelineSourceChanged(Project.ProjectSourceCodeWatcher.PipelineSourceChangedParams arg)
        {
            var path = arg.ProjectPaths[arg.ProjectPaths.Length - 1];

            var asb = AssemblyLoadContext.Default.LoadFromAssemblyPath(m_Project.GetAbsolutePath(path));
            var pipelineType = asb.GetTypes()
                .FirstOrDefault(t => typeof(IRenderPipeline).IsAssignableFrom(t));

            if (pipelineType != null)
            {
                m_RenderPipelineMutex.WaitOne();
                SetRenderPipeline((IRenderPipeline)Activator.CreateInstance(pipelineType));
                m_NextRenderPipelineAssembly = asb;
                m_RenderPipelineMutex.Release();
            }
        }

        void AssetSystemOnAssetUpdated(AssetUpdated assetUpdated)
        {
            bool updatePipeline;
            m_RenderPipeline.OnAssetUpdated(assetUpdated, out updatePipeline);
            m_UpdateRenderPipeline |= updatePipeline;
            m_NextRenderPipeline = m_RenderPipeline;
        }

        void LoadDefaultAssets()
        {
            m_AssetSystem.LoadShaderFromFile("main.vert", "main.vert.spv");
            m_AssetSystem.LoadShaderFromFile("main.frag", "main.frag.spv");
        }

        void RenderLoop()
        {
            var sw = Stopwatch.StartNew();
            var lastDate = sw.Elapsed.TotalSeconds;

            while (m_CanRender)
            {
                if (m_UpdateWindowSize)
                {
                    m_UpdateWindowSize = false;
                    m_GraphicsDevice.ResizeMainWindow((uint)m_Window.Width, (uint)m_Window.Height);
                }
                m_RenderPipelineMutex.WaitOne();
                var date = sw.Elapsed.TotalSeconds;
                var deltaTime = (float)(date - lastDate);

                m_RenderPipeline.RenderFrame(m_GraphicsDevice, deltaTime);
                m_RenderPipelineMutex.Release();
            }
        }

        #region IDisposable Support
        bool m_DisposedValue;

        void Dispose(bool disposing)
        {
            if (!m_DisposedValue)
            {
                if (disposing)
                {
                    m_CanRender = false;
                    m_RenderPipeline.Dispose();
                    m_Window?.Close();
                    m_ProjectSourceCodeWatcher?.Dispose();
                }

                m_DisposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
