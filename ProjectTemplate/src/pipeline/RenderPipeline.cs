﻿using Lightning.Assets;
using Lightning.RenderPipeline;
using System.Linq;
using Veldrid;

namespace ProjectRenderPipeline
{
    public class RenderPipeline : IRenderPipeline
    {
        Shader m_ShaderVertex;
        Shader m_ShaderFragment;
        Pipeline m_Pipeline;
        CommandList m_CommandList;

        public void CreateResources(ResourceFactory factory, GraphicsDevice gfxDevice, IAssetSystem assetSystem)
        {
            m_ShaderVertex = factory.CreateShader(new ShaderDescription(ShaderStages.Vertex, assetSystem.LoadShaderBytes("main.vert"), "VS"));
            m_ShaderFragment = factory.CreateShader(new ShaderDescription(ShaderStages.Fragment, assetSystem.LoadShaderBytes("main.frag"), "FS"));

            var vertexLayout = new VertexLayoutDescription(
                new VertexElementDescription("Position", VertexElementSemantic.Position, VertexElementFormat.Float2),
                new VertexElementDescription("Color", VertexElementSemantic.Color, VertexElementFormat.Float4));

            var pipelineDescription = new GraphicsPipelineDescription
            {
                BlendState = BlendStateDescription.SingleOverrideBlend,
                DepthStencilState = new DepthStencilStateDescription(
                    depthTestEnabled: true,
                    depthWriteEnabled: true,
                    comparisonKind: ComparisonKind.LessEqual
                    ),
                RasterizerState = new RasterizerStateDescription(
                    cullMode: FaceCullMode.Back,
                    fillMode: PolygonFillMode.Solid,
                    frontFace: FrontFace.Clockwise,
                    depthClipEnabled: true,
                    scissorTestEnabled: true
                    ),
                PrimitiveTopology = PrimitiveTopology.TriangleStrip,
                ResourceLayouts = System.Array.Empty<ResourceLayout>(),
                ShaderSet = new ShaderSetDescription(
                    vertexLayouts: new[] { vertexLayout },
                    shaders: new[] { m_ShaderVertex, m_ShaderFragment }
                    ),
                Outputs = gfxDevice.SwapchainFramebuffer.OutputDescription
            };
            m_Pipeline = factory.CreateGraphicsPipeline(pipelineDescription);

            m_CommandList = factory.CreateCommandList();
        }

        public void RenderFrame(GraphicsDevice gfxDevice, float deltaTime)
        {
            m_CommandList.Begin();

            m_CommandList.SetFramebuffer(gfxDevice.SwapchainFramebuffer);
            m_CommandList.SetFullViewports();
            m_CommandList.ClearColorTarget(0, RgbaFloat.Black);

            m_CommandList.SetPipeline(m_Pipeline);
            m_CommandList.Draw(4);

            m_CommandList.End();
            gfxDevice.SubmitCommands(m_CommandList);
            gfxDevice.SwapBuffers();
        }

        public void OnAssetUpdated(AssetUpdated assetUpdated, out bool updatePipeline)
        {
            updatePipeline = assetUpdated.ShaderPaths.Contains("main.frag")
                || assetUpdated.ShaderPaths.Contains("main.vert");
        }

        public void UpdateLogic(float deltaTime, InputSnapshot inputSnapshot, out bool exit)
        {
            exit = false;
            for (int i = 0, c = inputSnapshot.KeyEvents.Count; i < c; ++i)
            {
                var k = inputSnapshot.KeyEvents[i];
                if (k.Key == Key.Escape)
                    exit = true;
            }
        }


        #region IDisposable Support
        bool m_DisposedValue;

        void Dispose(bool disposing)
        {
            if (!m_DisposedValue)
            {
                if (disposing)
                {
                    m_CommandList.Dispose();
                    m_Pipeline.Dispose();
                    m_ShaderVertex.Dispose();
                    m_ShaderFragment.Dispose();
                }

                m_DisposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
