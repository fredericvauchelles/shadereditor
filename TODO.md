# TODO

## Asset Pipeline

- Reload project properties when changed
- Recompile all shaders when defines or include properties changed

## Tooling

- dotnet sample new: create new sample project
- dotnet project new: create new project from template

## Rendering

- D3D support
- find a way to expose a GUI (WPF form?) that can update dynamic values in RenderPipeline

## Logging

- Use coloring for console output for better readability
- Find a way to open file when clicking on a shader compilation log error
- Find a way to show error in text editor when shader compilation failed

## Documentation

- Getting started
- Overview