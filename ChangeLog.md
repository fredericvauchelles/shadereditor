# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

### Asset Pipeline - Shader
#### Added
 - Trigger compilation of shaders when an included file or the file itself is modified
 - Add include folder for shader compilation (authorize include outside project)
 - Hot reload shaders when compiled
 - Support shader: *.vert, *.frag, *.comp (Vertex, Fragment, Compute)
 - Support project defines for shader compilation
 - Qdded define BACKEND=VULKAN define for Vulkan backend

### Asset Pipeline - RenderPipeline
 - Hot reload render pipeline when compiled (*.dll)

### Project
 - Added project.json with project properties: Shader include folder, shader defines

### Logging
#### Added
 - Using Log categories in console output

### Tooling
#### Added
 - Added command: dotnet li sample prepare
 - Added command: dotnet li sample run
 - Added command: dotnet li template prepare
 - Added command: dotnet li template package