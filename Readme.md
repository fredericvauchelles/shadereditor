# Shader Editor

Shader Editor that reload shader files when editing them.

You can define your own rendering pipeline with multiple shader passes and compute shaders.

See the Samples for more informations

## Contributing

Run the shell.ps1 file and open the Visual Studio Solution from here.
The shell file will set up properly the paths for the tools.