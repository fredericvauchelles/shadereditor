#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "Lib/Commons/commons.glsl"
#include "Lib/Procedural/noise.glsl"

layout(set = 0) uniform Globals 
{
     vec4 time;              // (deltaTime, time, 1/deltaTime, 0)
     vec4 screenSize;        // (width, height, 1/width, 1/height)
} gl;

layout(location = 0) out vec4 outColor;

vec4 noisedColor(PositionInput posInput)
{
    vec3 p = vec3(posInput.positionNDC, gl.time.x * 0.05);

    vec2 q = vec2(  fbm(p.xy + vec2(0.0, 0.0), 0.3, 0.1, 10.0, 4.0),
                    fbm(p.xy + vec2(3.0, 4.0), 0.3, 0.1, 10.0, 4.0));
    vec2 q2 = vec2( fbm(p.xy + 4.0 * q + vec2(0.0, 0.0), 0.3, 0.1, 10.0, 4.0),
                    fbm(p.xy + 4.0 * q + vec2(3.0, 4.0), 0.3, 0.1, 10.0, 4.0));
    float r = fbm(p.xy + 4.0 * q2, 0.3, 0.1, 10.0, 4.0);
    // p += fbm3(p, 0.3, 0.1, 20.0, 4.0);
    // p += fbm3(p, 0.2, 0.1, 10.0, 4.0);
    // p += fbm3(p, 0.1, 0.1, 5.0, 4.0);
    return vec4(vec3(r), 1.0);
}

vec2 voronoiSDF2(vec2 p)
{
    const vec3 d = vec3(-1.0, 0.0, 1.0);
    const vec2 s = vec2(1.0, 0.3);

    vec2 fl = floor(p);
    
    vec2 r  = fl + random2(fl);
    vec2 distance = vec2(length(r - p), length((r - p) * s));

    vec2 tmpFL;

#define _ITERATION(offset)\
    tmpFL = fl + offset;\
    r  = tmpFL + random2(tmpFL);\
    distance = min(distance, vec2(length(r - p), length((r - p) * s)));

    _ITERATION(d.xx)
    _ITERATION(d.xy)
    _ITERATION(d.xz)
    _ITERATION(d.yz)
    _ITERATION(d.zz)
    _ITERATION(d.zy)
    _ITERATION(d.zx)
    _ITERATION(d.yx)

#undef _ITERATION

    return distance;
}

vec4 starColor(PositionInput posInput)
{
    const vec2 starSizeBase = vec2(0.1, 0.2);

    vec2 p = posInput.positionSS * 0.1;
    vec2 starSDF = voronoiSDF2(p);
    vec2 starSize = vec2(
        starSizeBase.x * (1 - 0.5 * random(floor(p))),
        starSizeBase.y * (1 - 0.9 * random(floor(p)))
    );
    vec2 starWeight2 = clamp(1.0 - starSDF / starSize, 0.0, 1.0);
    float starWeight = max(starWeight2.x, starWeight2.y);

    starWeight = starWeight * starWeight * (3.0 - 2.0 * starWeight);

    return vec4(vec3(starWeight), 1.0);
}

void main() {
    PositionInput posInput = GetFragPositionInput(gl_FragCoord.xyz, gl.screenSize);

//    outColor = noisedColor(posInput);
    outColor = starColor(posInput);
}