﻿using Lightning.Assets;
using Lightning.RenderPipeline;
using System;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using Veldrid;

namespace ProjectRenderPipeline
{
    public class RenderPipeline : IRenderPipeline
    {
        struct GlobalBuffer
        {
            public Vector4 Time;
            public Vector4 ScreenSize;
        }

        struct CameraBuffer
        {
            public Matrix4x4 View;
            public Matrix4x4 Projection;
            public Matrix4x4 ViewProjection;
            public Matrix4x4 InvView;
            public Matrix4x4 InvProjection;
            public Matrix4x4 InvViewProjection;

            public CameraBuffer(Matrix4x4 view, Matrix4x4 projection)
            {
                View = view;
                Projection = projection;
                ViewProjection = view * projection;
                Matrix4x4.Invert(View, out InvView);
                Matrix4x4.Invert(Projection, out InvProjection);
                Matrix4x4.Invert(ViewProjection, out InvViewProjection);
            }
        }

        Shader m_ShaderVertex;
        Shader m_ShaderFragment;
        Shader m_ShaderCompute;
        byte[] m_ShaderVertexBytes = Array.Empty<byte>();
        byte[] m_ShaderFragmentBytes = Array.Empty<byte>();
        byte[] m_ShaderComputeBytes = Array.Empty<byte>();
        Pipeline m_Pipeline;
        CommandList m_CommandList;

        ResourceLayout m_ResourceLayoutGraphics;
        ResourceSet m_ResourceSetGraphics;
        DeviceBuffer m_UniformBufferGlobals;
        DeviceBuffer m_UniformBufferCamera;

        ResourceLayout m_ResourceLayoutCompute;
        Pipeline m_PipelineCompute;
        Texture m_TextureGBuffer0;
        TextureView m_TextureGBuffer0View;
        ResourceSet m_ResourceSetCompute;

        float m_Time;

        public void CreateResources(ResourceFactory factory, GraphicsDevice gfxDevice, IAssetSystem assetSystem)
        {
            assetSystem.LoadShaderBytes("main.vert", ref m_ShaderVertexBytes);
            assetSystem.LoadShaderBytes("main.frag", ref m_ShaderFragmentBytes);
            assetSystem.LoadShaderBytes("main.comp", ref m_ShaderComputeBytes);

            if (m_ShaderVertexBytes.Length == 0
                || m_ShaderFragmentBytes.Length == 0
                || m_ShaderComputeBytes.Length == 0)
            {
                return;
            }

            // Create shaders
            m_ShaderVertex = factory.CreateShader(new ShaderDescription(ShaderStages.Vertex, m_ShaderVertexBytes, "VS"));
            m_ShaderFragment = factory.CreateShader(new ShaderDescription(ShaderStages.Fragment, m_ShaderFragmentBytes, "FS"));
            m_ShaderCompute = factory.CreateShader(new ShaderDescription(ShaderStages.Compute, m_ShaderComputeBytes, "main"));

            var vertexLayout = new VertexLayoutDescription(
                new VertexElementDescription("Position", VertexElementSemantic.Position, VertexElementFormat.Float2),
                new VertexElementDescription("TexCoord", VertexElementSemantic.TextureCoordinate, VertexElementFormat.Float4)
            );

            // Create buffer & textures
            m_UniformBufferGlobals = factory.CreateBuffer(new BufferDescription((uint)Marshal.SizeOf<GlobalBuffer>(), BufferUsage.UniformBuffer));
            m_UniformBufferCamera = factory.CreateBuffer(new BufferDescription((uint)Marshal.SizeOf<CameraBuffer>(), BufferUsage.UniformBuffer));

            m_TextureGBuffer0 = factory.CreateTexture(TextureDescription.Texture2D(
                gfxDevice.SwapchainFramebuffer.Width, gfxDevice.SwapchainFramebuffer.Height,
                1,
                1,
                PixelFormat.R32_G32_B32_A32_Float,
                TextureUsage.Sampled | TextureUsage.Storage
            ));
            m_TextureGBuffer0View = factory.CreateTextureView(m_TextureGBuffer0);

            // Create resource layouts && resource sets
            m_ResourceLayoutGraphics = factory.CreateResourceLayout(new ResourceLayoutDescription(
                new ResourceLayoutElementDescription("GBuffer0", ResourceKind.TextureReadOnly, ShaderStages.Fragment),
                new ResourceLayoutElementDescription("SS", ResourceKind.Sampler, ShaderStages.Fragment),
                new ResourceLayoutElementDescription("Globals", ResourceKind.UniformBuffer, ShaderStages.Fragment)
            ));
            m_ResourceSetGraphics = factory.CreateResourceSet(new ResourceSetDescription(
                m_ResourceLayoutGraphics,
                m_TextureGBuffer0View,
                gfxDevice.PointSampler,
                m_UniformBufferGlobals
            ));

            m_ResourceLayoutCompute = factory.CreateResourceLayout(new ResourceLayoutDescription(
                new ResourceLayoutElementDescription("GBuffer0", ResourceKind.TextureReadWrite, ShaderStages.Compute),
                new ResourceLayoutElementDescription("Globals", ResourceKind.UniformBuffer, ShaderStages.Compute),
                new ResourceLayoutElementDescription("Camera", ResourceKind.UniformBuffer, ShaderStages.Compute)
            ));

            m_ResourceSetCompute = factory.CreateResourceSet(new ResourceSetDescription(
                m_ResourceLayoutCompute,
                m_TextureGBuffer0View,
                m_UniformBufferGlobals,
                m_UniformBufferCamera
            ));

            // Create pipelines
            m_PipelineCompute = factory.CreateComputePipeline(new ComputePipelineDescription(
                m_ShaderCompute,
                m_ResourceLayoutCompute,
                1, 1, 1
            ));

            var pipelineDescription = new GraphicsPipelineDescription
            {
                BlendState = BlendStateDescription.SingleOverrideBlend,
                DepthStencilState = DepthStencilStateDescription.DepthOnlyLessEqual,
                RasterizerState = RasterizerStateDescription.Default,
                PrimitiveTopology = PrimitiveTopology.TriangleStrip,
                ResourceLayouts = new[] { m_ResourceLayoutGraphics },
                ShaderSet = new ShaderSetDescription(
                    vertexLayouts: new[] { vertexLayout },
                    shaders: new[] { m_ShaderVertex, m_ShaderFragment }
                    ),
                Outputs = gfxDevice.SwapchainFramebuffer.OutputDescription
            };
            m_Pipeline = factory.CreateGraphicsPipeline(pipelineDescription);

            m_CommandList = factory.CreateCommandList();
        }

        public void RenderFrame(GraphicsDevice gfxDevice, float deltaTime)
        {
            if (m_CommandList == null)
                return;

            m_Time += deltaTime;
            var frameBuffer = gfxDevice.SwapchainFramebuffer;
            var globalsBuffer = new GlobalBuffer
            {
                Time = new Vector4(
                    deltaTime,
                    m_Time,
                    1.0f / deltaTime, 0.0f
                ),
                ScreenSize = new Vector4(
                    frameBuffer.Width, frameBuffer.Height,
                    1.0f / frameBuffer.Width, 1.0f / frameBuffer.Height
                )
            };
            var cameraBuffer = new CameraBuffer(
                Matrix4x4.CreateTranslation(new Vector3(0, 1.0f, 1.0f)),
                Matrix4x4.CreatePerspectiveFieldOfView(
                    90.0f * (float)Math.PI / 360.0f,
                    gfxDevice.SwapchainFramebuffer.Width / (float)gfxDevice.SwapchainFramebuffer.Height,
                    0.1f,
                    1000.0f
                )
            );

            m_CommandList.Begin();

            m_CommandList.UpdateBuffer(m_UniformBufferGlobals, 0, ref globalsBuffer);
            m_CommandList.UpdateBuffer(m_UniformBufferCamera, 0, ref cameraBuffer);

            m_CommandList.SetPipeline(m_PipelineCompute);
            m_CommandList.SetComputeResourceSet(0, m_ResourceSetCompute);
            m_CommandList.Dispatch(gfxDevice.SwapchainFramebuffer.Width, gfxDevice.SwapchainFramebuffer.Height, 1);

            m_CommandList.SetFramebuffer(gfxDevice.SwapchainFramebuffer);
            m_CommandList.SetFullViewports();
            m_CommandList.SetFullScissorRects();
            m_CommandList.ClearColorTarget(0, RgbaFloat.Black);
            m_CommandList.SetPipeline(m_Pipeline);
            m_CommandList.SetGraphicsResourceSet(0, m_ResourceSetGraphics);
            m_CommandList.Draw(4);

            m_CommandList.End();
            gfxDevice.SubmitCommands(m_CommandList);
            gfxDevice.SwapBuffers();
        }

        public void OnAssetUpdated(AssetUpdated assetUpdated, out bool updatePipeline)
        {
            updatePipeline = assetUpdated.ShaderPaths.Contains("main.frag")
                || assetUpdated.ShaderPaths.Contains("main.vert")
                || assetUpdated.ShaderPaths.Contains("main.comp");
        }

        public void UpdateLogic(float deltaTime, InputSnapshot inputSnapshot, out bool exit)
        {
            exit = false;
            for (int i = 0, c = inputSnapshot.KeyEvents.Count; i < c; ++i)
            {
                var k = inputSnapshot.KeyEvents[i];
                if (k.Key == Key.Escape)
                    exit = true;
            }
        }

        #region IDisposable Support
        bool m_DisposedValue;

        void Dispose(bool disposing)
        {
            if (!m_DisposedValue)
            {
                if (disposing)
                {
                    m_ResourceLayoutCompute?.Dispose();
                    m_PipelineCompute?.Dispose();
                    m_TextureGBuffer0?.Dispose();
                    m_TextureGBuffer0View?.Dispose();
                    m_ResourceSetCompute?.Dispose();

                    m_ResourceLayoutGraphics?.Dispose();
                    m_UniformBufferGlobals?.Dispose();
                    m_UniformBufferCamera?.Dispose();
                    m_ResourceSetGraphics?.Dispose();

                    m_CommandList?.Dispose();
                    m_Pipeline?.Dispose();
                    m_ShaderVertex?.Dispose();
                    m_ShaderFragment?.Dispose();
                    m_ShaderCompute?.Dispose();
                }

                m_DisposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
