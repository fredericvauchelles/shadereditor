#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "Lib/Commons/commons.glsl"
#include "Lib/Commons/debug.glsl"

layout(set = 0, binding = 0) uniform texture2D _GBuffer0;
layout(set = 0, binding = 1) uniform sampler sampler_linear;
layout(set = 0, binding = 2) uniform Globals 
{
     vec4 time;              // (deltaTime, time, 1/deltaTime, 0)
     vec4 screenSize;        // (width, height, 1/width, 1/height)
} gl;

layout(location = 0) out vec4 outColor;

void main() {
    PositionInput posInput = GetFragPositionInput(gl_FragCoord.xyz, gl.screenSize);

    vec4 gBuffer0 = texture(sampler2D(_GBuffer0, sampler_linear), posInput.positionNDC);
    float depth = gBuffer0.x;
    uint matId = floatBitsToUint(gBuffer0.y);

    outColor = vec4(vec3(fract(depth * 0.1)), 1.0);
    //outColor = vec4(GetIndexColor(int(matId)), 1.0);
    //outColor = gBuffer0;
}