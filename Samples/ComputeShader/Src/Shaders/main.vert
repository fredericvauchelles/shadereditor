#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    vec2 positionNDC = vec2((gl_VertexIndex & 1), ((gl_VertexIndex >> 1) & 1)) * 2.0 - 1.0;
    gl_Position = vec4(positionNDC, 0.0, 1.0);
} 