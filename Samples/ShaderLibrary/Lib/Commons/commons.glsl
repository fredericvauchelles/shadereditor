#ifndef INCLUDE_COMMONS_H
#define INCLUDE_COMMONS_H

struct PositionInput
{
    vec2    positionNDC;
    ivec2   positionSS;
    vec3    positionWS;
    float   linearDepth;
};

PositionInput GetFragPositionInput(vec3 fragCoord, vec4 screenSize)
{
    PositionInput r;
    r.positionSS = ivec2(fragCoord.x, screenSize.y - fragCoord.y);
    r.linearDepth = fragCoord.z;
    r.positionNDC = r.positionSS * screenSize.zw;

    return r; 
}

PositionInput GetPositionInput(ivec2 positionSS, vec4 screenSize, mat4x4 invViewProj)
{
    PositionInput r;
    r.positionSS = positionSS;
    r.positionNDC = r.positionSS * screenSize.zw;
    
    vec4 positionCS = vec4(r.positionNDC * 2.0 - 1.0, 0.0, 1.0);
    vec4 pWS = invViewProj * positionCS;
    pWS.xyz /= pWS.w;

    r.linearDepth = pWS.w;
    r.positionWS = pWS.xyz;

    return r; 
}

#endif