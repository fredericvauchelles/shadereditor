#ifndef INCLUDE_DEBUG_H
#define INCLUDE_DEBUG_H

vec3 GetIndexColor(int index)
{
    vec3 outColor = vec3(1.0, 0.0, 0.0);

    if (index == 0)
        outColor = vec3(1.0, 0.5, 0.5);
    else if (index == 1)
        outColor = vec3(0.5, 1.0, 0.5);
    else if (index == 2)
        outColor = vec3(0.5, 0.5, 1.0);
    else if (index == 3)
        outColor = vec3(1.0, 1.0, 0.5);
    else if (index == 4)
        outColor = vec3(1.0, 0.5, 1.0);
    else if (index == 5)
        outColor = vec3(0.5, 1.0, 1.0);
    else if (index == 6)
        outColor = vec3(0.25, 0.75, 1.0);
    else if (index == 7)
        outColor = vec3(1.0, 0.75, 0.25);
    else if (index == 8)
        outColor = vec3(0.75, 1.0, 0.25);
    else if (index == 9)
        outColor = vec3(0.75, 0.25, 1.0);
    else if (index == 10)
        outColor = vec3(0.25, 1.0, 0.75);
    else if (index == 11)
        outColor = vec3(0.75, 0.75, 0.25);
    else if (index == 12)
        outColor = vec3(0.75, 0.25, 0.75);
    else if (index == 13)
        outColor = vec3(0.25, 0.75, 0.75);
    else if (index == 14)
        outColor = vec3(0.25, 0.25, 0.75);
    else if (index == 15)
        outColor = vec3(0.75, 0.25, 0.25);

    return outColor;
}

#endif