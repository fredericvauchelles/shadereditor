struct MaterialSpec
{
    uint matId;
};

struct SDFSample
{
    float distance;
    MaterialSpec mat;
};

// ------------------------
// Operators
// ------------------------

SDFSample SDFOr(SDFSample l, SDFSample r)
{
    return l.distance < r.distance ? l : r;
}

// ------------------------
// Shapes
// ------------------------

SDFSample SDFSamplePlane(MaterialSpec mat, vec3 position, vec3 center)
{
    SDFSample r;
    r.distance = position.y - center.y;
    r.mat = mat;
    return r;
}

SDFSample SDFSampleSphere(MaterialSpec mat, vec3 position, vec3 center, float radius)
{
    SDFSample r;
    r.distance = length(position - center) - radius;
    r.mat = mat;
    return r;
}

// ------------------------
// Shapes
// ------------------------

SDFSample EvaluateSDF(in vec3 position);
bool RayMarch(in vec3 position, in vec3 r, in float near, in float far, out SDFSample hit)
{
    float t = near;
    for(int i = 0; i < 64 && t <= far; ++i)
    {
        vec3 p = position + r * t;
        float precis = 0.00005 * t;
        hit = EvaluateSDF(p);
        if (hit.distance < precis)
            return true;
        t += max(hit.distance, 0.005 * t);
    }
    
    return false;
}