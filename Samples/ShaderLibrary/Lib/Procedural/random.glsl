#ifndef INCLUDE_RANDOM_H
#define INCLUDE_RANDOM_H

#include "Lib/Commons/macros.glsl"

// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct(uint m)
{
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat(m);       // Range [1:2]
    return f - 1.0;                        // Range [0:1]
}

uint hash(uint x)
{
    x += (x << 10u);
    x ^= (x >>  6u);
    x += (x <<  3u);
    x ^= (x >> 11u);
    x += (x << 15u);
    return x;
}

uint hashA(uint x)
{
    x = (x >> 1u) | (x & 1) << 31;
    return hash(x);
}

uint hashB(uint x)
{
    x = (x >> 2u) | (x & 3) << 30;
    return hash(x);
}

uint hashC(uint x)
{
    x = (x >> 3u) | (x & 7) << 29;
    return hash(x);
}

#define HASHNAME hash
#define RANDNAME random
#include "Lib/Procedural/random.impl.glsl"
#undef HASHNAME
#undef RANDNAME

#define HASHNAME hashA
#define RANDNAME randomA
#include "Lib/Procedural/random.impl.glsl"
#undef HASHNAME
#undef RANDNAME

#define HASHNAME hashB
#define RANDNAME randomB
#include "Lib/Procedural/random.impl.glsl"
#undef HASHNAME
#undef RANDNAME

#define HASHNAME hashC
#define RANDNAME randomC
#include "Lib/Procedural/random.impl.glsl"
#undef HASHNAME
#undef RANDNAME

#define RANDS(inType)\
vec2 random2(inType v) { return vec2(random(v), randomA(v)                        ); }\
vec3 random3(inType v) { return vec3(random(v), randomA(v), randomB(v)            ); }\
vec4 random4(inType v) { return vec4(random(v), randomA(v), randomB(v), randomC(v)); }

RANDS(float)
RANDS(vec2)
RANDS(vec3)
RANDS(vec4)
#undef RANDS

#endif