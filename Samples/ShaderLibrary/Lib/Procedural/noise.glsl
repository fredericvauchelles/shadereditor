#include "Lib/Procedural/random.glsl"

// -----------------------------------------
// Noises
// -----------------------------------------

#define RANDNAME random
#define GDNOISENAME gdnoise
#include "Lib/Procedural/noise.impl.glsl"
#undef RANDNAME
#undef GDNOISENAME

#define RANDNAME randomA
#define GDNOISENAME gdnoiseA
#include "Lib/Procedural/noise.impl.glsl"
#undef RANDNAME
#undef GDNOISENAME

#define RANDNAME randomB
#define GDNOISENAME gdnoiseB
#include "Lib/Procedural/noise.impl.glsl"
#undef RANDNAME
#undef GDNOISENAME

#define RANDNAME randomC
#define GDNOISENAME gdnoiseC
#include "Lib/Procedural/noise.impl.glsl"
#undef RANDNAME
#undef GDNOISENAME

// -----------------------------------------
// Fractal Brownian Motion
// -----------------------------------------

#define NOISENAME gdnoise
#define FBMNAME fbm
#include "Lib/Procedural/fbm.impl.glsl"
#undef NOISENAME
#undef FBMNAME

#define NOISENAME gdnoiseA
#define FBMNAME fbmA
#include "Lib/Procedural/fbm.impl.glsl"
#undef NOISENAME
#undef FBMNAME

#define NOISENAME gdnoiseB
#define FBMNAME fbmB
#include "Lib/Procedural/fbm.impl.glsl"
#undef NOISENAME
#undef FBMNAME

#define NOISENAME gdnoiseC
#define FBMNAME fbmC
#include "Lib/Procedural/fbm.impl.glsl"
#undef NOISENAME
#undef FBMNAME

#define FBMS(inType)\
vec2 fbm2(inType v, float a, float g, float f, float l) { return vec2(fbm(v, a, g, f, l), fbmA(v, a, g, f, l)                                           ); }\
vec3 fbm3(inType v, float a, float g, float f, float l) { return vec3(fbm(v, a, g, f, l), fbmA(v, a, g, f, l), fbmB(v, a, g, f, l)                      ); }\
vec4 fbm4(inType v, float a, float g, float f, float l) { return vec4(fbm(v, a, g, f, l), fbmA(v, a, g, f, l), fbmB(v, a, g, f, l), fbmC(v, a, g, f, l) ); }

FBMS(float)
FBMS(vec2)
FBMS(vec3)
#undef FBMS

float voronoiSDF(vec2 p)
{
    const vec3 d = vec3(-1.0, 0.0, 1.0);

    vec2 fl = floor(p);
    
    vec2 r  = fl + random2(fl);
    float distance = length(r - p);

    vec2 tmpFL;

#define _ITERATION(offset)\
    tmpFL = fl + offset;\
    r  = tmpFL + random2(tmpFL);\
    distance = min(distance, length(r - p));

    _ITERATION(d.xx)
    _ITERATION(d.xy)
    _ITERATION(d.xz)
    _ITERATION(d.yz)
    _ITERATION(d.zz)
    _ITERATION(d.zy)
    _ITERATION(d.zx)
    _ITERATION(d.yx)

#undef _ITERATION

    return distance;
}