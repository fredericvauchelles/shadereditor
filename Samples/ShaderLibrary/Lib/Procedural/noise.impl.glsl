#if defined(RANDNAME) && defined(GDNOISENAME)

float GDNOISENAME(float v)
{
    float fr = fract(v);
    float fl = floor(v);
    fr = fr * fr * (3.0 - 2.0 * fr);
    return mix(RANDNAME(fl), RANDNAME(fl + 1), fr);
}

float GDNOISENAME(vec2 v)
{
    const vec2 d = vec2(0.0, 1.0);
    vec2 fr = fract(v);
    vec2 fl = floor(v);
    fr = fr * fr * (3.0 - 2.0 * fr);
    return mix(
            mix(RANDNAME(fl + d.xx), RANDNAME(fl + d.yx), fr.x), 
            mix(RANDNAME(fl + d.xy), RANDNAME(fl + d.yy), fr.x), 
        fr.y
    );
}

float GDNOISENAME(vec3 v)
{
    const vec2 d = vec2(0.0, 1.0);
    vec3 fr = fract(v);
    vec3 fl = floor(v);
    fr = fr * fr * (3.0 - 2.0 * fr);
    
    return mix(
        mix(
            mix(RANDNAME(fl + d.xxx), RANDNAME(fl + d.yxx), fr.x), 
            mix(RANDNAME(fl + d.xyx), RANDNAME(fl + d.yyx), fr.x), 
        fr.y
        ),
        mix(
            mix(RANDNAME(fl + d.xxy), RANDNAME(fl + d.yxy), fr.x), 
            mix(RANDNAME(fl + d.xyy), RANDNAME(fl + d.yyy), fr.x), 
        fr.y
        ),
        fr.z
    );
}

#endif