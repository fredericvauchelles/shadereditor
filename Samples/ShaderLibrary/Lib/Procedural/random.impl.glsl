#if (defined(HASHNAME) && defined(RANDNAME))

uint HASHNAME(uvec2 v) { return HASHNAME(v.x ^ HASHNAME(v.y)                                ); }
uint HASHNAME(uvec3 v) { return HASHNAME(v.x ^ HASHNAME(v.y) ^ HASHNAME(v.z)                ); }
uint HASHNAME(uvec4 v) { return HASHNAME(v.x ^ HASHNAME(v.y) ^ HASHNAME(v.z) ^ HASHNAME(v.w)); }

float RANDNAME(float x) { return floatConstruct(HASHNAME(floatBitsToUint(x))); }
float RANDNAME(vec2  v) { return floatConstruct(HASHNAME(floatBitsToUint(v))); }
float RANDNAME(vec3  v) { return floatConstruct(HASHNAME(floatBitsToUint(v))); }
float RANDNAME(vec4  v) { return floatConstruct(HASHNAME(floatBitsToUint(v))); }

#endif