#if defined(NOISENAME) && defined(FBMNAME)

float FBMNAME(float v, float a, float g, float f, float l)
{
    float r = 0;
    for (int i = 0; i < 5; ++i)
    {
        r += a * NOISENAME(v * f);
        f *= l;
        a *= g;
    }
    return r;
}

float FBMNAME(vec2 v, float a, float g, float f, float l)
{
    float r = 0;
    for (int i = 0; i < 5; ++i)
    {
        r += a * NOISENAME(v * f);
        f *= l;
        a *= g;
    }
    return r;
}

float FBMNAME(vec3 v, float a, float g, float f, float l)
{
    float r = 0;
    for (int i = 0; i < 5; ++i)
    {
        r += a * NOISENAME(v * f);
        f *= l;
        a *= g;
    }
    return r;
}

#endif